import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.yaml.snakeyaml.Yaml;
import java.util.*;
import java.io.*;

public class Main {

	public Map general;
	public BotConfig enten;
	public BotConfig hicks;
	public BotConfig rms;
	public BotConfig windoof;
	private String dsgvo = "Dieser Bot speichert keine persönlichen Daten.";

	public static void main(String[] args) {
		String config_filename = "config/config.yml";
		Main main = new Main();
		main.initConfig(config_filename);
		main.run();
	}

	public Main() {
		this.general = new HashMap<>();
	}

	public void run() {
		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			if (this.enten.activated) {
				TelegramLongPollingBot bot = new EntenBot(this.enten.username, this.enten.token, (String) this.general.get("git"), this.dsgvo);
				telegramBotsApi.registerBot(bot);
			}
			if (this.hicks.activated) {
				TelegramLongPollingBot bot = new HicksBot(this.hicks.username, this.hicks.token, (String) this.general.get("git"), this.dsgvo);
				telegramBotsApi.registerBot(bot);
			}
			if (this.rms.activated) {
				TelegramLongPollingBot bot = new RMSBot(this.rms.username, this.rms.token, (String) this.general.get("git"), this.dsgvo);
				telegramBotsApi.registerBot(bot);
			}
			if (this.windoof.activated) {
				TelegramLongPollingBot bot = new WindoofBot(this.windoof.username, this.windoof.token, (String) this.general.get("git"), this.dsgvo);
				telegramBotsApi.registerBot(bot);
			}
		} catch (TelegramApiException e){
			BotLogger.error("LOGTAG", e);
		}
	}

	public void initConfig(String configfile) {
		File file;
		InputStream input;
		try {
			file = new File(configfile);
			input = new FileInputStream(file);
		} catch (FileNotFoundException fnfe) {
			System.out.println("Missing config file!");
			System.exit(1);
			return;
		}
		Yaml yaml = new Yaml();
		Map<String, Map> map = yaml.load(input);;
		this.general = map.get("General");
		this.general.put("git", (String) map.get("General").get("git"));
		this.enten = new BotConfig(
				(boolean) map.get("EntenBot").get("activated"),
				(String) map.get("EntenBot").get("username"),
				(String) map.get("EntenBot").get("token")
				);
		this.hicks = new BotConfig(
				(boolean) map.get("HicksBot").get("activated"),
				(String) map.get("HicksBot").get("username"),
				(String) map.get("HicksBot").get("token")
				);
		this.rms = new BotConfig(
				(boolean) map.get("RMSBot").get("activated"),
				(String) map.get("RMSBot").get("username"),
				(String) map.get("RMSBot").get("token")
				);
		this.windoof = new BotConfig(
				(boolean) map.get("WindoofBot").get("activated"),
				(String) map.get("WindoofBot").get("username"),
				(String) map.get("WindoofBot").get("token")
				);
	}
}
